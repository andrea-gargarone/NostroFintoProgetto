/**
 * 
 */
package model;
import java.util.*;
//import cards.Card;
/**
 * @author Luca
 *  This is our player, we'll put him on a list
 */
public class Player {
	
	private String playerName; 
	
	private List<Position> boxes;
	
	private String humanOrAlien;
	
	private int gameId;
	
	private List<String> objects; //I can't remember, we use objects as string or what? 
								  //We use objects as Card (they are objectCard)

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public List<Position> getBoxes() {
		return boxes;
	}

	public void setBoxes(List<Position> boxes) {
		this.boxes = boxes;
	}

	public String getHumanOrAlien() {
		return humanOrAlien;
	}

	public void setHumanOrAlien(String humanOrAlien) {
		this.humanOrAlien = humanOrAlien;
	}

	public int getGameId() {
		return gameId;
	}

	public void setGameId(int gameId) {
		this.gameId = gameId;
	}

	public List<String> getObjects() {
		return objects;
	}

	public void setObjects(List<String> objects) {
		this.objects = objects;
	}

}

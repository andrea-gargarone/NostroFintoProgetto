/**
 * 
 */
package model;

/**
 * @author Luca
 * @param Position(int row, char coloumn) constructor
 * @param String sector for dangerous, white, alienbase ecc
 * @param boolean visitable says if it is visitable or not
 * 
 */
public class Position {


	final private int row;
	
	final private char coloumn;
	
	private String sector;
	
	private boolean visitable;

	public Position(int row, char coloumn) {
		super();
		this.row = row;
		this.coloumn = coloumn;
	}

	public int getRow() {
		return row;
	}

	public char getColoumn() {
		return coloumn;
	}
	
	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}

	public boolean isVisitable() {
		return visitable;
	}

	public void setVisitable(boolean visitable) {
		this.visitable = visitable;
	}
	
}

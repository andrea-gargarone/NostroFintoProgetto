/**
 * 
 */
package model;

import java.util.HashMap;

import java.util.Map;

import model.exception.WrongInputException;

/**
 * 
 * @author Luca
 * @newMap(String mapName) to create new hashmap<string,position>. It allows Galilei, RandomMap ecc..
 * @createGalileiMap() create the whole map, setting all sectors to Dangerous and visitable first. Other sectors set apart
 */ 
public class GameMap {

	
	public Map<String, Position> platform;
	
	private String positionName;            //Contains boxes value like A1, B4 ecc before putting them on the HashMap
		
	public void newMap(String mapName){                     //Easyextendmethod if galilei create galilei else throw.. you can add as many maps as you like
		if(mapName.equalsIgnoreCase("Galilei"))
			createGalileiMap();
		else throw new WrongInputException("Insert a valid map name");
		
	}

	private void createGalileiMap() {
        
		platform = new HashMap<String, Position>();
     
     	for(int row = 1; row<15; row++) {
     		for(char col = 'a'; col < 'x'; col++){
    		
     			positionName = String.valueOf(col)+String.valueOf(row);		//Create String A13, B1 ecc
     			Position toHash = new Position(row,col);       //Create Position toHash calling constructor
     			toHash.setSector("Dangerous");
     			toHash.setVisitable(true);
     			platform.put(positionName,toHash);
    		  		 
    		 
     		}
    	
     	}
     
     	/*Hard coding for empty sector
     	 * 
     	 */
     	platform.get("a1").setSector("Empty");
  
     	platform.get("a7").setSector("Empty");
    
     	platform.get("a8").setSector("Empty");
   
     	platform.get("b7").setSector("Empty");
    
     	platform.get("d1").setSector("Empty");
     
     	platform.get("d4").setSector("Empty");
     
     	platform.get("d6").setSector("Empty");
     
     	platform.get("d7").setSector("Empty");
     
     	platform.get("e1").setSector("Empty");
     
     	platform.get("e7").setSector("Empty");
    
     	platform.get("e14").setSector("Empty");
   
     	platform.get("f12").setSector("Empty");
     
     	platform.get("f13").setSector("Empty");
     
     	platform.get("f14").setSector("Empty");
     
     	platform.get("h5").setSector("Empty");
     
     	platform.get("h10").setSector("Empty");
     
     	platform.get("h11").setSector("Empty");
     
     	platform.get("i6").setSector("Empty");
     
     	platform.get("i12").setSector("Empty");
     	
     	platform.get("j7").setSector("Empty");
     	
     	platform.get("j12").setSector("Empty");
    
     	platform.get("k7").setSector("Empty");
     	
     	platform.get("k13").setSector("Empty");
     	
     	platform.get("l7").setSector("Empty");
     	
     	platform.get("m7").setSector("Empty");
     	
     	platform.get("n7").setSector("Empty");
     	
     	platform.get("o1").setSector("Empty");
     	
     	platform.get("o3").setSector("Empty");

     	platform.get("o4").setSector("Empty");
     	
     	platform.get("r10").setSector("Empty");
     	
     	platform.get("r11").setSector("Empty");
     	
     	platform.get("r14").setSector("Empty");
     	
     	platform.get("s1").setSector("Empty");
     	
     	platform.get("s3").setSector("Empty");
     	
     	platform.get("s10").setSector("Empty");
     	
     	platform.get("s11").setSector("Empty");
     	
     	platform.get("s14").setSector("Empty");
     	
     	platform.get("t1").setSector("Empty");
     	
     	platform.get("t3").setSector("Empty");
     	
     	platform.get("t4").setSector("Empty");
     	
     	platform.get("t9").setSector("Empty");
     	
     	platform.get("t10").setSector("Empty");
     
     	platform.get("v7").setSector("Empty");
     	
     	platform.get("w1").setSector("Empty");
     	
     	platform.get("w7").setSector("Empty");
     	
     	platform.get("w8").setSector("Empty");
	
	 /*Hard coding for silence sector
	  * 
	  */
	
     	platform.get("a4").setSector("Silence");
     	
     	platform.get("a5").setSector("Silence");
     	
     	platform.get("a6").setSector("Silence");
     	
     	platform.get("a9").setSector("Silence");
     	
     	platform.get("a10").setSector("Silence");
	
     	platform.get("a11").setSector("Silence");
     	
     	platform.get("a12").setSector("Silence");
     	
     	platform.get("a13").setSector("Silence");
     	
     	platform.get("b5").setSector("Silence");
     	
     	platform.get("b10").setSector("Silence");
     	
     	platform.get("c1").setSector("Silence");
     	
     	platform.get("c14").setSector("Silence");
     	
     	platform.get("d10").setSector("Silence");
     	
     	platform.get("d14").setSector("Silence");
     	
     	platform.get("e2").setSector("Silence");
     	
     	platform.get("e12").setSector("Silence");
     	
     	platform.get("f1").setSector("Silence");
     	
     	platform.get("f10").setSector("Silence");
     	
     	platform.get("g7").setSector("Silence");
     	
     	platform.get("g12").setSector("Silence");
     	
     	platform.get("g14").setSector("Silence");
	
     	platform.get("h1").setSector("Silence");
     	
     	platform.get("h2").setSector("Silence");
     	
     	platform.get("h3").setSector("Silence");
     	
     	platform.get("h7").setSector("Silence");
     	
     	platform.get("h14").setSector("Silence");
	
     	platform.get("i1").setSector("Silence");
     	
     	platform.get("i9").setSector("Silence");
     	
     	platform.get("i14").setSector("Silence");
     	
     	platform.get("j1").setSector("Silence");
     	
     	platform.get("j14").setSector("Silence");
     	
     	platform.get("k2").setSector("Silence");
     	
     	platform.get("k5").setSector("Silence");
     	
     	platform.get("k9").setSector("Silence");
     	
     	platform.get("k11").setSector("Silence");
     	
     	platform.get("l2").setSector("Silence");
     	
     	platform.get("l4").setSector("Silence");
     	
     	platform.get("l9").setSector("Silence");
     	
     	platform.get("l11").setSector("Silence");
     	
     	platform.get("l14").setSector("Silence");
     	
     	platform.get("m2").setSector("Silence");
     	
     	platform.get("m5").setSector("Silence");
     	
     	platform.get("m9").setSector("Silence");
     	
     	platform.get("m11").setSector("Silence");
     	
     	platform.get("n3").setSector("Silence");
     	
     	platform.get("n14").setSector("Silence");
     	
     	platform.get("o5").setSector("Silence");
	
     	platform.get("o9").setSector("Silence");
     	
     	platform.get("o14").setSector("Silence");
     	
     	platform.get("p1").setSector("Silence");
     	
     	platform.get("p3").setSector("Silence");
     	
     	platform.get("p4").setSector("Silence");
     	
     	platform.get("p12").setSector("Silence");
     	
     	platform.get("q1").setSector("Silence");
     	
     	platform.get("q4").setSector("Silence");
     	
     	platform.get("q6").setSector("Silence");
     	
     	platform.get("q11").setSector("Silence");
     	
     	platform.get("q14").setSector("Silence");
     	
     	platform.get("r1").setSector("Silence");
     	
     	platform.get("r4").setSector("Silence");
     	
     	platform.get("r6").setSector("Silence");
     	
     	platform.get("r7").setSector("Silence");
     	
     	platform.get("r8").setSector("Silence");
     	
     	platform.get("r12").setSector("Silence");
     	
     	platform.get("t7").setSector("Silence");
     	
     	platform.get("t8").setSector("Silence");
     	
     	platform.get("t14").setSector("Silence");
     	
     	platform.get("u1").setSector("Silence");
     	
     	platform.get("u5").setSector("Silence");
     	
     	platform.get("u12").setSector("Silence");
     	
     	platform.get("v1").setSector("Silence");
     	
     	platform.get("v8").setSector("Silence");
     	
     	platform.get("w3").setSector("Silence");
     	
     	platform.get("w4").setSector("Silence");
     	
     	platform.get("w5").setSector("Silence");
     	
     	platform.get("w6").setSector("Silence");
     	
     	platform.get("w10").setSector("Silence");
     	
     	platform.get("w11").setSector("Silence");
     	
     	platform.get("w12").setSector("Silence");
     	
     	/* Setting special sector */
     	 
	
     	platform.get("b2").setSector("EscapeHatch");
     	
     	platform.get("v2").setSector("EscapeHatch");
     	
     	platform.get("b13").setSector("EscapeHatch");
     	
     	platform.get("v13").setSector("EscapeHatch");
     	
     	platform.get("l6").setSector("AlienBase");
     	platform.get("l6").setVisitable(false);
     	platform.get("l8").setSector("HumanBase");
     	platform.get("l8").setVisitable(false);
	
     	/* Setting not visitable ones*/
     	
     	for(String change : platform.keySet()){
     		if( platform.get(change).getSector().equalsIgnoreCase("Empty") )
     			platform.get(change).setVisitable(false);
     	}
	
	}

	
	/**
	 * @param hatchDamaged: receive string with position of hatch and change to damage
	 * 
	 */
	public void hatchDamaged(String hatchDamaged){
		if(platform.get(hatchDamaged).getSector().equalsIgnoreCase("EscapeHatch")){
			platform.get(hatchDamaged).setSector("EscapeHatchDamaged");
			platform.get(hatchDamaged).setVisitable(false);
	
		}else{
			throw new WrongInputException("This isn't an Hatch, u can't change it! This error will never happen");
		}
	}

	
	
	
	
	
	
	
	
	
}

package cards;

/**
 * @author Andrea Gargarone
 */

public class ObjectCard extends Card{	
	
	public ObjectCard(int i){	
		
		String[] typeObjectCard = {"Adrenaline", "Adrenaline", "Attack", "Attack", 
				"Teleport", "Teleport", "Sedatives", "Sedatives", "Sedatives", 
				"Spotlight", "Spotlight", "Defense"};
		
		this.symbolCard = typeObjectCard[i];
	}
}

package cards;

/**
 * 
 * @author Andrea Gargarone
 *
 */

public interface Deck{
	
	public void shuffle();
	
	public Card draw();
	
	public boolean isEmpty();

}

/*
public void show()
{
	for (ObjectCard card : this.objectCards)
    {
        System.out.println(card.getSymbolCard());
    }
}*/
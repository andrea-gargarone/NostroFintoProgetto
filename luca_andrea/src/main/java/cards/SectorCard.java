package cards;

/**
 * @author Andrea Gargarone
 */

public class SectorCard extends Card {
	
    private boolean objectSymbol;

    public SectorCard(int i) { 
    	
    	String[] typeSectorCard = {"NoiseYourSector", "NoiseYourSector", "NoiseYourSector", 
    			"NoiseYourSector", "NoiseYourSector", "NoiseYourSector", "NoiseYourSector", 
    			"NoiseYourSector", "NoiseYourSector", "NoiseYourSector", "NoiseAnySector", 
    			"NoiseAnySector", "NoiseAnySector", "NoiseAnySector", "NoiseAnySector", 
    			"NoiseAnySector", "NoiseAnySector", "NoiseAnySector", "NoiseAnySector", 
    			"NoiseAnySector", "Silence", "Silence", "Silence", "Silence", "Silence", };
    	
    	boolean[] objectSymbolSectorCard = {true, true, true, true, false, false, false, false, 
    			false, false, true, true, true, true, false, false, false, false, false, false, 
    			false, false, false, false, false, };
    	
    	this.symbolCard = typeSectorCard[i];
    	this.objectSymbol = objectSymbolSectorCard[i];
    	
    }
    
    public SectorCard() {    	
    	
    }

	public void setObjectSymbol(boolean b)	{
		this.objectSymbol = b;
	}

	public String getSectorCardValue() {
		String var = "";
		var += getSymbolCard() + " - " + getObjectSymbol();
		return var;
	}
	
	public boolean getObjectSymbol() {
		return this.objectSymbol;
	}
}

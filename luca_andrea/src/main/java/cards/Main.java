package cards;

/**
 * @author Andrea Gargarone
 */

public class Main
{

    public static void main(String[] args) {
        
        /**
         * Generate deck of sector cards
             	
    	Deck mazzo = new SectorCardDeck();
    	mazzo.show();
    	mazzo.shuffle();
    	System.out.println("\n\n\nMazzo mescolato:\n");
    	mazzo.show();
    	
    	//c is the card that I draw
    	SectorCard c = (SectorCard) mazzo.draw();
    	String card = c.getSectorCardValue();
    	System.out.println("\n\n" + card + "\n\n");
    	mazzo.show();
    	
    	/*
    	 * Genarate deck of hatch cards

    	HatchCardDeck mazzo = new HatchCardDeck();
    	mazzo.show();
    	mazzo.shuffle();
    	System.out.println("\n\n");
    	mazzo.show();
    	*/
        
    	
    	/*
    	 * ObjectCardDeck mazzo = new ObjectCardDeck();
    	mazzo.show();
    	mazzo.shuffle();
    	System.out.println("\n");
    	mazzo.show();
    	*/
    }
}
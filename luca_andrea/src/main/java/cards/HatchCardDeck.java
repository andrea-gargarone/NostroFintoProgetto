package cards;

/**
 * @author Andrea Gargarone
 * 
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class HatchCardDeck implements Deck{
	
	private ArrayList<HatchCard> hatchCards = new ArrayList<HatchCard>();
	
	public HatchCardDeck(){	
		
		for(int i = 0; i < 6; i++) {
			
			hatchCards.add(new HatchCard(i));
		}
	}
	
	//this method mixes the cards
	public void shuffle() {
		
        long seed = System.nanoTime();
        Collections.shuffle(hatchCards, new Random(seed));
    }
	
	//This method return the card on the top of the deck and remove it
	public HatchCard draw(){
		
		return hatchCards.remove(0);
	}

	//Return if the deck is empty
	public boolean isEmpty(){
		
		if(hatchCards.isEmpty()){
			
			return true;
			
		}
		else{
			
			return false;
		}
	}
}

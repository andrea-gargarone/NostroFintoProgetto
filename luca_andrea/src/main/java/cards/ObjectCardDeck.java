package cards;

import cards.Deck;

/**
 * @author Andrea Gargarone
 */


import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class ObjectCardDeck implements Deck
{
	private ArrayList<ObjectCard> objectCards = new ArrayList<ObjectCard>();
	
	public ObjectCardDeck()	{
		for(int i = 0; i < 12; i++) {
			
			objectCards.add(new ObjectCard(i));
		}
	}
	
	//this method mixes the cards
	public void shuffle()
    {
        long seed = System.nanoTime();
        Collections.shuffle(objectCards, new Random(seed));
    }

	//This method return the card on the top of the deck and remove it
	public ObjectCard draw()
	{
		return objectCards.remove(0);
	}
	
	//Return if the deck is empty
	public boolean isEmpty()
	{
		if(objectCards.isEmpty())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

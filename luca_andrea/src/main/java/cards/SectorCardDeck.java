package cards;

/**
 * @author Andrea Gargarone
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class SectorCardDeck implements Deck
{
	private ArrayList<SectorCard> sectorCard = new ArrayList<SectorCard>();
	
	public SectorCardDeck()
	{				
		for(int i = 0; i < 25; i++)
		{
			sectorCard.add(new SectorCard(i));
		}
		//this.shuffle();  //At the creation the deck must be shuffle
	}
	
	public void shuffle()
    {
        long seed = System.nanoTime();
        Collections.shuffle(sectorCard, new Random(seed));
    }
	
	//This method return the card on the top of the deck and remove it
	public SectorCard draw()
	{
		return sectorCard.remove(0);
	}
	
	//Verify if the deck is empty
	public boolean isEmpty()
	{
		if(sectorCard.isEmpty())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
